// Load plugins
import gulp from 'gulp';
const { series, parallel, src, dest, task } = gulp;
import rename from 'gulp-rename';

task('build',
  parallel(
    bulmaswatch, bulmajs, fontawesome_css, fontawesome_font, particles, bulma_tooltip
));
task('build').description = 'Build npm dependencies';

function bulmaswatch() {
  return src('node_modules/bulmaswatch/superhero/bulmaswatch.min.css')
    .pipe(rename('bulmaswatch.css'))
    .pipe(dest('source/stylesheets/'));
};

function bulmajs() {
  const base = 'node_modules/@vizuaalog/bulmajs/dist/';
  return src([ base+'tabs.js', base+'navbar.js', base+'panelTabs.js' ], { buffer: false })
    .pipe(dest('source/javascripts/'));
};

function fontawesome_css() {
  return src('node_modules/@fortawesome/fontawesome-free/css/all.min.css', { buffer: false })
    .pipe(dest('source/stylesheets/fontawesome/css/'));
};

function fontawesome_font() {
  return src('node_modules/@fortawesome/fontawesome-free/webfonts/*',
    { base: 'node_modules/@fortawesome/fontawesome-free/',
      // buffer: false, // https://github.com/gulpjs/gulp/issues/2768#issuecomment-2030071554
      encoding: false }) // https://github.com/gulpjs/gulp/issues/2766
    .pipe(dest('source/stylesheets/fontawesome/'));
};

function particles() {
  return src('node_modules/particles.js/particles.js', { buffer: false })
    .pipe(rename('particlesjs.js'))
    .pipe(dest('source/javascripts/'));
};

function bulma_tooltip() {
  return src('node_modules/@creativebulma/bulma-tooltip/dist/bulma-tooltip.min.css')
    .pipe(rename('bulma-tooltip.min.css'))
    .pipe(dest('source/stylesheets/'));
};
